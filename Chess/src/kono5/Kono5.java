package kono5;

import static game.core.PieceColor.BLACK;
import static game.core.PieceColor.WHITE;

import game.core.Game;
import game.players.IPlayer;
import game.players.Neznaika;
import kono5.board.Kono5Board;
import kono5.pieces.Stone;
import kono5.players.Julio;
import kono5.players.Miga;

public class Kono5 extends Game {
    private static final int BOARD_SIZE = 5;

    static {
        Game.addPlayer(Kono5.class, IPlayer.HOMO_SAPIENCE);
        Game.addPlayer(Kono5.class, new Neznaika());
        Game.addPlayer(Kono5.class, new Julio());
        Game.addPlayer(Kono5.class, new Miga());
    }

    public Kono5() {
        super(new Kono5Board());
        initBoardDefault();

        board.setWhitePlayer(IPlayer.HOMO_SAPIENCE);
        board.setBlackPlayer(new Neznaika());
    }

    @Override
    public void initBoardDefault() {
        super.initBoard(BOARD_SIZE, BOARD_SIZE);

        for (int k = 0; k < BOARD_SIZE; k++) {
            new Stone(board.getSquare(k, 0), BLACK);
            new Stone(board.getSquare(k, BOARD_SIZE - 1), WHITE);
        }

        new Stone(board.getSquare(0, 1), BLACK);
        new Stone(board.getSquare(BOARD_SIZE - 1, 1), BLACK);

        new Stone(board.getSquare(0, BOARD_SIZE - 2), WHITE);
        new Stone(board.getSquare(BOARD_SIZE - 1, BOARD_SIZE - 2), WHITE);
    }
}
