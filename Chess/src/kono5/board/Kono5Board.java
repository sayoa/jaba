package kono5.board;

import game.core.Board;
import game.core.GameOver;
import game.players.IPlayer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

public class Kono5Board extends Board {
    private boolean isOver = false;
    private Runnable onGameStartCallback = () -> {};
    private Runnable onGameOverCallback = () -> {};

    public Kono5Board() {
        super();
    }

    // Automatically makes a turn for a robo-players but exits when it's
    // human's turn
    private void playUntilHumansTurn(boolean getColorFirst) throws GameOver {
        while (true) {
            if (getColorFirst) {
                moveColor = getNextColor(moveColor);
            }
            IPlayer player = players.get(moveColor);
            if (player == IPlayer.HOMO_SAPIENCE)
                break; // Allow human to make a turn with input device

            player.doMove(this, moveColor);
            if (!getColorFirst) {
                moveColor = getNextColor(moveColor);
            }
        }
    }

    // Creates pop-up with game result
    private void handleGameOver(GameOver go) {
        isOver = true;
        System.out.println("Well, the game is over: " + go.result);
        Display display = Display.getCurrent();
        Shell shell = new Shell(display);

        // Create a MessageBox
        MessageBox messageBox = new MessageBox(shell, SWT.OK);
        messageBox.setText("The Game Is Over!");

        String wordResult = "<ERR>";
        switch (go.result) {
            case WHITE_WIN: wordResult = "White Won!"; break;
            case BLACK_WIN: wordResult = "Black Won!"; break;
            case DRAWN:     wordResult = "It's a DRAW!"; break;
            case UNKNOWN:   wordResult = "Something unknown happen..."; break;
        }
        messageBox.setMessage(wordResult);

        // Open the MessageBox and wait for user input
        messageBox.open();
        shell.dispose();  // Close the shell and release resources
        onGameOverCallback.run();
    }

    // Handles the game turn of events
    private void doSteps(boolean getColorFirst) {
        try {
            if (!isOver) {
                playUntilHumansTurn(getColorFirst);
            }
        }
        catch (GameOver e) {
            handleGameOver(e);
        }
    }

    @Override
    public void startGame() {
        isOver = false;
        onGameStartCallback.run();
        doSteps(false);
    }

    @Override
    public void changeMoveColor() {
        doSteps(true);
    }

    public void onGameOver(Runnable cb) {
        this.onGameOverCallback = cb;
    }

    public void onGameStart(Runnable cb) {
        this.onGameStartCallback = cb;
    }

    public void whenGameOver() {
        handleGameOver(new GameOver(history.getResult()));
    }
}
