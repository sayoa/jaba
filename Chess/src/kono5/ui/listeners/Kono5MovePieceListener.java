package kono5.ui.listeners;

import game.core.*;
import game.ui.listeners.IGameListner;
import org.eclipse.swt.graphics.Cursor;

import game.ui.GameBoard;

// fork MovePieceListener
public class Kono5MovePieceListener implements IGameListner {
    private Piece selectedPiece;

    private Square selectedSquare;

    private Cursor savedCursor;

    private final Board board;

    private final GameBoard boardPanel;

    private Runnable onGameOverCallback = () -> {};

    public Kono5MovePieceListener(GameBoard boardPanel, Runnable goc) {
        this.board = boardPanel.board;
        this.boardPanel = boardPanel;
        this.onGameOverCallback = goc;
    }

    private void resetAfterUserMove() {
        selectedPiece = null;
        selectedSquare = null;

        boardPanel.setCursor(savedCursor);

        board.setBoardChanged();
        boardPanel.redraw();
    }

    // Remove piece from the board and allow user to hover it above the board to choose the target spot
    @Override
    public void mouseDown(Square mouseSquare, int button) {
        if (mouseSquare.isEmpty())
            return;

        selectedPiece = mouseSquare.getPiece();
        if (selectedPiece.getColor() != board.getMoveColor())
            return;

        selectedSquare = mouseSquare;
        selectedSquare.removePiece();

        savedCursor = boardPanel.getCursor();

        boardPanel.pieceToCursor(selectedPiece);

        board.setBoardChanged();
        boardPanel.redraw();
    }

    // Place the piece back to the board
    public void mouseUp(Square mouseSquare, int button) {
        // deny placing it outside the board
        if (selectedSquare == null)
            return;

        selectedSquare.setPiece(selectedPiece);

        if (selectedPiece.isCorrectMove(mouseSquare)) {
            Move move = selectedPiece.makeMove(selectedSquare, mouseSquare);
            try {
                move.doMove();
            } catch (GameOver e) {
                board.history.addMove(move);
                board.history.setResult(e.result);
                resetAfterUserMove();
                onGameOverCallback.run();
                return;
            }

            board.history.addMove(move);
            board.changeMoveColor();
        }

        resetAfterUserMove();
    }
}