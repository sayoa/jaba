package kono5.ui;

import game.ui.listeners.IGameListner;
import kono5.board.Kono5Board;
import kono5.ui.listeners.Kono5MovePieceListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;

import game.core.Game;
import game.core.Piece;
import game.core.PieceColor;
import game.core.Square;
import game.ui.AsiaBoard;
import game.ui.images.GameImages;
import game.ui.listeners.MovePieceListener;
import mingmang.pieces.Stone;

public class Kono5BoardPanel extends AsiaBoard {
    public Kono5BoardPanel(Composite composite, Game game) {
        super(composite, game.board);
        Kono5Board k5board = (Kono5Board)game.board;

        k5board.onGameOver(() -> listener = IGameListner.EMPTY);
        k5board.onGameStart(() -> {
            listener = new Kono5MovePieceListener(this, k5board::whenGameOver);
        });
    }

    @Override
    public Piece getPiece(Square square, PieceColor color) {
        return new Stone(square, color);
    }

    @Override
    public Image getPieceImage(Piece piece, PieceColor color) {
        return color == PieceColor.WHITE ? GameImages.wStone : GameImages.bStone;
    }
}
