package kono5.ui;

import org.eclipse.swt.widgets.Composite;

import game.ui.GamePanel;
import kono5.Kono5;

public class Kono5GamePanel extends GamePanel {

    public Kono5GamePanel(Composite parent) {
        super(parent, new  Kono5());

        Kono5BoardPanel gameBoard = new Kono5BoardPanel(this, game);
        insertSquares(gameBoard);
    }
}
