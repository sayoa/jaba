package kono5.pieces;

import game.core.Move;
import game.core.Piece;
import game.core.PieceColor;
import game.core.Square;
import kono5.moves.SimpleMove;

public class Stone extends Piece {
    public Stone(Square square, PieceColor color) {
        super(square, color);
    }

    @Override
    public boolean isCorrectMove(Square... squares) {
        Square target = squares[0];
        return target.isEmpty() && (
            square.v + 1 == target.v && square.h + 1 == target.h ||
            square.v + 1 == target.v && square.h - 1 == target.h ||
            square.v - 1 == target.v && square.h + 1 == target.h ||
            square.v - 1 == target.v && square.h -1 == target.h
        );
    }

    @Override
    public PieceColor getColor() {
        return super.getColor();
    }

    @Override
    public Move makeMove(Square... squares) {
        return new SimpleMove(squares);
    }

    @Override
    public String toString() {
        return "";
    }
}
