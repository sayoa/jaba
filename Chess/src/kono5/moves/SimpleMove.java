package kono5.moves;

import game.core.*;
import game.core.moves.ITransferMove;

public class SimpleMove implements ITransferMove {
    // Какая фигура перемещается.
    protected final Piece piece;

    // Откуда перемещается.
    protected final Square source;

    // Куда перемещается.
    protected final Square target;

    public SimpleMove(Square[] squares) {
        source = squares[0];
        target = squares[1];

        piece = source.getPiece();
    }

    @Override
    public Square getTarget() {
        return target;
    }

    @Override
    public Square getSource() {
        return source;
    }

    private boolean white_win_detection(Square square) {
        return square.h==0 || (square.h==1 && (square.v==0 || square.v==4));
    }

    private boolean white_no_empty_detection(Square source) {
        Board board = source.getBoard();

        for (int v = 0; v < 5; v++) {
            if (board.getSquare(v, 0).isEmpty())
                return false;
        }
        if (board.getSquare(0, 1).isEmpty() || board.getSquare(4, 1).isEmpty())
            return false;
        return true;
    }

    private boolean black_win_detection(Square square) {
        return square.h==4 || (square.h==3 && (square.v==0 || square.v==4));
    }

    private boolean black_no_empty_detection(Square source) {
        Board board = source.getBoard();

        for (int v = 0; v < 5; v++) {
            if (board.getSquare(v, 4).isEmpty())
                return false;
        }
        if (board.getSquare(0, 3).isEmpty() || board.getSquare(4, 3).isEmpty())
            return false;
        return true;
    }

    @Override
    public void doMove() throws GameOver {

        boolean game_over = false;

        piece.moveTo(target);

        if (piece.getColor().toString().equals("WHITE"))
            game_over = white_win_detection(target) && white_no_empty_detection(source);
        else if (piece.getColor().toString().equals("BLACK"))
            game_over = black_win_detection(target) && black_no_empty_detection(source);

        if (game_over)
            throw new GameOver(GameResult.win(piece));
    }

    @Override
    public void undoMove() {
        piece.moveTo(source);
    }

    @Override
    public String toString() {
        return "" + piece + source + "-" + target;
    }

    @Override
    public Piece getPiece() {
        return piece;
    }
}
