package kono5.players;

import java.util.Collections;
import java.util.List;

import game.core.*;
import game.core.moves.ITransferMove;
import game.players.MovePiecePlayer;


public class Julio extends MovePiecePlayer {
    private final int maxMoves;

    public Julio() {
        this(80);
    }

    public Julio(int maxMoves) {
        this.maxMoves = maxMoves;
    }

    @Override
    public String getName() {
        return "Жулио";
    }

    @Override
    public String getAuthorName() {
        return "Демьянов Иван";
    }

    @Override
    public void doMove(Board board, PieceColor color) throws GameOver {
        List<Move> correctMoves = getCorrectMoves(board, color);

        if (correctMoves.isEmpty())
            return;

        Collections.shuffle(correctMoves);

        Move Julio_selected_Move = correctMoves.get(0);

        for (int i = 0; i < correctMoves.size(); i++) {

            ITransferMove move = (ITransferMove) correctMoves.get(i);

            Square source = move.getSource();
            Square target = move.getTarget();

            if (color.toString().equals("WHITE"))
                if (target.h - source.h < 0)
                    Julio_selected_Move = move;

            if (color.toString().equals("BLACK"))
                if (target.h - source.h > 0)
                    Julio_selected_Move = move;
        }

        try {
            Julio_selected_Move.doMove();
        } catch (GameOver e) {
            // Сохраняем в истории игры последний сделанный ход
            // и результат игры.
            board.history.addMove(Julio_selected_Move);
            board.history.setResult(e.result);

            // Просим обозревателей доски показать
            // положение на доске, сделанный ход и
            // результат игры.
            board.setBoardChanged();

            throw new GameOver(e.result);
        }

        // Сохраняем ход в истории игры.
        board.history.addMove(Julio_selected_Move);

        // Просим обозревателей доски показать
        // положение на доске, сделанный ход и
        // результат игры.
        board.setBoardChanged();

        // Для отладки ограничим количество ходов в игре.
        // После этого результат игры ничья.
        if (board.history.getMoves().size() > maxMoves) {
            // Сохраняем в истории игры последний сделанный ход
            // и результат игры.
            board.history.setResult(GameResult.DRAWN);

            // Сообщаем что игра закончилась ничьей.
            throw new GameOver(GameResult.DRAWN);
        }
    }

    @Override
    public String toString() {
        return getName();
    }
}
