package kono5.players;

import java.util.*;

import game.core.*;
import game.core.moves.ITransferMove;
import game.players.MovePiecePlayer;

/**
 * TODO Панферов Виктор
 */
public class Miga extends MovePiecePlayer {
    private final int maxMoves;

    public Miga() {
        this(80);
    }

    public Miga(int maxMoves) {
        this.maxMoves = maxMoves;
    }

    @Override
    public String getName() {
        return "Мига";
    }

    @Override
    public String getAuthorName() {
        return "Панферов Виктор";
    }

    static private int calculateBlackMoveScore(Move m) {
        ITransferMove move = (ITransferMove) m;
        int th = move.getTarget().h;
        int tv = move.getTarget().v;
        int sh = move.getSource().h;
        int sv = move.getSource().v;

        // already in the winning place
        if (sh == 4 || sh == 3 && (sv == 0 || sv == 4)) return -10;
        // about to step into the winning place
        if (th == 4 || th == 3 && (tv == 0 || tv == 4)) return 20;

        if (sh == 3 && (sv == 1 || sv == 2 || sv == 3)) return -8;
        if (th == 3 && (tv == 1 || tv == 2 || tv == 3)) return 18;

        if (sh == 2) return -5;
        if (th == 2) return 15;

        if (sh == 1) return -3;
        if (th == 1) return 10;

        if (sh == 0) return -3;
        if (th == 0) return -15;

        return 0;
    }
    static private int calculateWhiteMoveScore(Move m) {
        ITransferMove move = (ITransferMove) m;
        int th = move.getTarget().h;
        int tv = move.getTarget().v;
        int sh = move.getSource().h;
        int sv = move.getSource().v;

        // already in the winning place
        if (sh == 0 || sh == 1 && (sv == 0 || sv == 4)) return -10;
        // about to step into the winning place
        if (th == 0 || th == 1 && (tv == 0 || tv == 4)) return 20;

        if (sh == 1 && (sv == 1 || sv == 2 || sv == 3)) return -8;
        if (th == 1 && (tv == 1 || tv == 2 || tv == 3)) return 18;

        if (sh == 2) return -5;
        if (th == 2) return 15;

        if (sh == 3) return -3;
        if (th == 3) return 10;

        if (sh == 4) return -3;
        if (th == 4) return -15;
        return 0;
    }

    @Override
    public void doMove(Board board, PieceColor color) throws GameOver {
        List<Move> correctMoves = getCorrectMoves(board, color);

        if (correctMoves.isEmpty())
            throw new GameOver(color == PieceColor.BLACK ? GameResult.WHITE_WIN : GameResult.BLACK_WIN);

        Map<Integer, List<Move>> sortedMoves = new HashMap<Integer, List<Move>>();
        int maxScore = Integer.MIN_VALUE;
        for (Move move : correctMoves) {
            int score = color == PieceColor.BLACK ? calculateBlackMoveScore(move) : calculateWhiteMoveScore(move);

            if (!sortedMoves.containsKey(score)) {
                sortedMoves.put(score, new LinkedList<Move>());
            }
            List<Move> kmoves = sortedMoves.get(score);
            kmoves.add(move);
            maxScore = Math.max(score, maxScore);
        }

        Collections.shuffle(sortedMoves.get(maxScore));
        Move bestMove = sortedMoves.get(maxScore).get(0);

        try {
            bestMove.doMove();
        } catch (GameOver e) {
            // Сохраняем в истории игры последний сделанный ход
            // и результат игры.
            board.history.addMove(bestMove);
            board.history.setResult(e.result);

            // Просим обозревателей доски показать
            // положение на доске, сделанный ход и
            // результат игры.
            board.setBoardChanged();

            throw new GameOver(e.result);
        }

        // Сохраняем ход в истории игры.
        board.history.addMove(bestMove);

        // Просим обозревателей доски показать
        // положение на доске, сделанный ход и
        // результат игры.
        board.setBoardChanged();

        // Для отладки ограничим количество ходов в игре.
        // После этого результат игры ничья.
        if (board.history.getMoves().size() > maxMoves) {
            // Сохраняем в истории игры последний сделанный ход
            // и результат игры.
            board.history.setResult(GameResult.DRAWN);

            // Сообщаем что игра закончилась ничьей.
            throw new GameOver(GameResult.DRAWN);
        }
    }

    @Override
    public String toString() {
        return getName();
    }
}
